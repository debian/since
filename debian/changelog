since (1.1-7) unstable; urgency=medium

  * Update Vcs-* headers for move to Salsa.
  * Drop no more needed Testsuite header.
  * Remove trailing whitespace from ancient debian/changelog entries.
  * Add lintian override for duplicate word "long" in dev_t.patch.
  * Check for the nocheck build option/profile in override_dh_auto_test.
    (Closes: #938980)
  * Use $AUTOPKGTEST_TMP instead of $ADTTMP in test suite.
  * Set "Rules-Requires-Root: no".
  * Declare compliance with Debian Policy 4.4.0. (No changes needed.)
  * Bump debhelper compatibility level to 12.
    + Build-depend on "debhelper-compat (= 12)" to replace debian/compat.
  * Drop debian/source/lintian-overrides, no more needed.
  * Convert debian/copyright to machine-readable DEP5 format.
  * Fix compiler warning about unsigned vs long unsigned int in fprintf.

 -- Axel Beckert <abe@debian.org>  Sat, 31 Aug 2019 14:31:19 +0200

since (1.1-6) unstable; urgency=medium

  * Enable all hardening build flags.
  * Declare compliance with Debian Policy 3.9.8.
  * Change XS-Testsuite header name to Testsuite.
  * Switch Vcs-* headers to https:// and cgit.
  * Add patch to fix spelling error found by Lintian.

 -- Axel Beckert <abe@debian.org>  Fri, 20 Jan 2017 01:28:17 +0100

since (1.1-5) unstable; urgency=medium

  * Make adequate messages trigger a failure in the adequate autopkgtest
  * Output content of .since in case of test failure
  * Merge changelog entry from impatient NMU by Anibal Monsalve Salazar
    + Ignore dev_t.patch from NMU, use my DEP-3 compliant variant
  * Import updated dev_t.patch by Svante Signell to make it work on
    GNU/Hurd, too. Thanks! (Closes: #591386)
    + Rewrite patch description to make it more clear and to mention Hurd.

 -- Axel Beckert <abe@debian.org>  Sat, 24 May 2014 22:06:29 +0200

since (1.1-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * For mips/mipsel ABIO32, define int d_dev as unsigned long int.
    Add dev_t.patch.
    Patch by Aleksandar Zlicic.
    Closes: #742940

 -- Anibal Monsalve Salazar <anibal@debian.org>  Tue, 08 Apr 2014 05:35:53 +0100

since (1.1-4) unstable; urgency=medium

  * Switch to a git-buildpackage compatible VCS layout
  * Bump Standards-Version to 3.9.5 (no changes)
  * Update upstream's e-mail address in debian/copyright.
  * Add (a rather minimal) DEP-12 debian/upstream/metadata file.
  * Update patch header of hurd-path-max.patch to conform to DEP-3.
  * Add DEP-8 automatic package testing.
    + Use part of the test suite also at build-time

 -- Axel Beckert <abe@debian.org>  Sat, 29 Mar 2014 04:54:07 +0100

since (1.1-3) unstable; urgency=low

  * Bump debhelper compatibility to 9
    - Update versioned debhelper build-dependency
  * Patch upstream Makefile to
    - don't overwrite already set variables
    - pass additional variables (CPPFLAGS, LDFLAGS)
  * Streamline debian/rules
    - use dh_auto_{build,clean}
    - use debian/since.manpages instead of dh_installman parameter
    - switch to minimal dh7 style debian/rules, but don't run dh_auto_install
  * Fixes the following lintian warnings:
    - hardening-no-relro
    - hardening-no-fortify-functions
    - vcs-field-not-canonical
    - debian-rules-missing-recommended-target
  * Rename debian/since.* to debian/*
  * Bump Standards-Version to 3.9.4 (no further changes)

 -- Axel Beckert <abe@debian.org>  Fri, 17 May 2013 01:27:31 +0200

since (1.1-2) unstable; urgency=low

  * Fix FTBFS on GNU/Hurd
    - add patch hurd-path-max.patch based on fix for #257087.

 -- Axel Beckert <abe@debian.org>  Mon, 02 Aug 2010 13:11:55 +0200

since (1.1-1) unstable; urgency=low

  * New upstream release (Closes: #590939)
    - [debian/rules] remove call to reallyclean make target as it no more
      exists.
    - [debian/copyright] Updated; License changed to GPLv3+
  * New Maintainer, thanks Felipe for all his work!
  * Move to git as VCS, adapt Vcs-Headers
  * Bump Standards-Version to 3.9.1 (no changes necessary)
  * Replace "dh_clean -k" by "dh_prep" and bump debhelper compatibility to 7
  * Switch to Source Format "3.0 (quilt)"
  * Add dependency on ${misc:Depends}

 -- Axel Beckert <abe@debian.org>  Mon, 02 Aug 2010 00:22:53 +0200

since (0.5-5) unstable; urgency=low

  * Change maintainer e-mail address.
  * Fix lintian warnings
    - debian-rules-ignores-make-clean-error
  * Bump Standards-Version to 3.7.3 (without modification).
  * Added Vcs-* fields (debian/control).
  * Added Homepage field (debian/control).
  * Added debian/watch.
  * Update debhelper compat to 5.
    - Update Build-Deps accordingly.
  * Updating information in debian/copyright.

 -- Felipe Augusto van de Wiel (faw) <faw@debian.org>  Sat, 26 Jan 2008 21:35:09 -0600

since (0.5-4) unstable; urgency=low

  * Adding a version to the debhelper depends (debian/control).
  * Bumping Standards-Version to 3.7.2 (no changes needed).

 -- Felipe Augusto van de Wiel (faw) <felipe@cathedrallabs.org>  Tue, 22 Aug 2006 01:31:22 -0300

since (0.5-3) unstable; urgency=low

  * New maintainer. (Closes: #313638).
  * Bumping standards version to 3.6.2.2.

 -- Felipe Augusto van de Wiel (faw) <felipe@cathedrallabs.org>  Fri, 20 Jan 2006 14:18:21 -0200

since (0.5-2) unstable; urgency=low

  * Set maintainer to Debian QA Group
  * Updated Standards-Version
  * Removed version in build-dep on debhelper
  * Add debian/compat

 -- Luk Claes <luk@debian.org>  Sat, 10 Sep 2005 09:57:34 +0000

since (0.5-1) unstable; urgency=low

  * New upstream version.
  * New maintainer (closes: #201365).
  * debhelper in Build-Depends (closes: #190527).
  * Updated to standards version 3.6.0.

 -- Luk Claes <luk.claes@ugent.be>  Fri, 18 Jul 2003 15:47:38 +0200

since (0.3-1) unstable; urgency=low

  * New upstream.

 -- Michael-John Turner <mj@debian.org>  Fri, 11 May 2001 22:57:46 +0200

since (0.2-1) unstable; urgency=low

  * New upstream.
  * Updated to policy 3.2.0 and changed build slightly.

 -- Michael-John Turner <mj@debian.org>  Mon, 20 Nov 2000 21:01:26 +0200

since (0.1-2) unstable; urgency=low

  * Updated to policy 3.0.1

 -- Michael-John Turner <mj@debian.org>  Fri, 17 Sep 1999 14:30:47 +0200

since (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Michael-John Turner <mj@debian.org>  Wed, 19 May 1999 18:28:18 +0200
